﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EncrytpionDecryption
{
    public partial class frmQuestionMaker : Form
    {
        public frmQuestionMaker ()
        {
            InitializeComponent();
        }

        private void btnCLose_Click ( object sender, EventArgs e )
        {
            this.Close();
        }

        private void btnSave_Click ( object sender, EventArgs e )
        {
            if(ChecktheCriteria() == true)
            CreatingQuestionAnswersAndChoices();
            else
                MessageBox.Show( "Please Give All necssary information "+Environment.NewLine+"about this Question","Invalid Question",MessageBoxButtons.OK,MessageBoxIcon.Error );
        }

        private bool ChecktheCriteria ()
        {
            bool result = false;
            if ( txtChoice1.Text != string.Empty &&
                txtChoice2.Text != string.Empty &&
                txtChoice3.Text != string.Empty &&
                txtChoice4.Text != string.Empty &&
                txtQuestion.Text != string.Empty )
            {
                if ( rtnAnswer1.Checked == true || rtnAnswer2.Checked == true || rtnAnswer3.Checked == true || rtnAnswer4.Checked == true )
                    result = true;
            }


            return result;
        }

        private void CreatingQuestionAnswersAndChoices ()
        {
            /// Writing the Question in Question file by separatingQustion by ? Question mark.
            /// 

            using ( StreamWriter writer = File.AppendText( "Questions.txt" ) )
            {
                writer.Write( txtQuestion.Text + "?" );
                //  writer.WriteLine(textBox1.Text);

                writer.Close();
                //stream.Close();
            }

            /// now I've to save the MCQS in Choices File.
            /// 
            //FileStream McqsStream = new FileStream( "test2.txt", FileMode.Append, FileAccess.Write );
            //StreamWriter Mcqswriter = new StreamWriter( McqsStream );
            using ( StreamWriter w = File.AppendText( "MCQS.txt" ) )
            {
                w.Write( txtChoice1.Text + "_ " + txtChoice2.Text + " _ " + txtChoice3.Text + " _ " + txtChoice4.Text + "?" );


                w.Close();
                // stream.Close();
            }

            /// writing answer list
            /// 
            using ( StreamWriter w = File.AppendText( "UserAnswerFile.txt" ) )
            {
                if ( rtnAnswer1.Checked == true )
                    w.Write( "1," );
                else
                    if ( rtnAnswer2.Checked == true )
                        w.Write( "2," );
                    else
                        if ( rtnAnswer3.Checked == true )
                            w.Write( "3," );
                        else
                            if ( rtnAnswer4.Checked == true )
                                w.Write( "4," );
                w.Close();
                // stream.Close();
            }
        }
    }
}
