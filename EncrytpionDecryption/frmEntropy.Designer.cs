﻿namespace EncrytpionDecryption
{
    partial class frmEntropy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvLettersEntropy = new System.Windows.Forms.DataGridView();
            this.colLetters = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LetterQuantityInformation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvWordsProbability = new System.Windows.Forms.DataGridView();
            this.coldgvWords = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WordsQuantityInformation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldgvOccurrence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTotalEntropy = new System.Windows.Forms.Label();
            this.lblAverage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLettersEntropy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWordsProbability)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLettersEntropy
            // 
            this.dgvLettersEntropy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvLettersEntropy.BackgroundColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLettersEntropy.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLettersEntropy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLettersEntropy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLetters,
            this.LetterQuantityInformation,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLettersEntropy.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvLettersEntropy.GridColor = System.Drawing.Color.Blue;
            this.dgvLettersEntropy.Location = new System.Drawing.Point(119, 22);
            this.dgvLettersEntropy.Name = "dgvLettersEntropy";
            this.dgvLettersEntropy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLettersEntropy.Size = new System.Drawing.Size(471, 341);
            this.dgvLettersEntropy.TabIndex = 14;
            this.dgvLettersEntropy.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLettersEntropy_CellContentClick);
            // 
            // colLetters
            // 
            this.colLetters.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colLetters.DataPropertyName = "LetterName";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe Print", 15.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Purple;
            this.colLetters.DefaultCellStyle = dataGridViewCellStyle2;
            this.colLetters.HeaderText = "Letras";
            this.colLetters.Name = "colLetters";
            this.colLetters.ReadOnly = true;
            // 
            // LetterQuantityInformation
            // 
            this.LetterQuantityInformation.DataPropertyName = "LetterQuantityInformation";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe Print", 14.25F);
            this.LetterQuantityInformation.DefaultCellStyle = dataGridViewCellStyle3;
            this.LetterQuantityInformation.HeaderText = "Q.I";
            this.LetterQuantityInformation.Name = "LetterQuantityInformation";
            this.LetterQuantityInformation.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "LetterEntropy";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 15.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Purple;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "Entropy";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 130;
            // 
            // dgvWordsProbability
            // 
            this.dgvWordsProbability.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvWordsProbability.BackgroundColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWordsProbability.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvWordsProbability.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWordsProbability.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coldgvWords,
            this.WordsQuantityInformation,
            this.coldgvOccurrence});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvWordsProbability.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvWordsProbability.GridColor = System.Drawing.Color.Blue;
            this.dgvWordsProbability.Location = new System.Drawing.Point(119, 22);
            this.dgvWordsProbability.Name = "dgvWordsProbability";
            this.dgvWordsProbability.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWordsProbability.Size = new System.Drawing.Size(471, 341);
            this.dgvWordsProbability.TabIndex = 13;
            this.dgvWordsProbability.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWordsProbability_CellContentClick);
            // 
            // coldgvWords
            // 
            this.coldgvWords.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.coldgvWords.DataPropertyName = "WordsName";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe Print", 13.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Purple;
            this.coldgvWords.DefaultCellStyle = dataGridViewCellStyle7;
            this.coldgvWords.HeaderText = "Palavras";
            this.coldgvWords.Name = "coldgvWords";
            this.coldgvWords.ReadOnly = true;
            // 
            // WordsQuantityInformation
            // 
            this.WordsQuantityInformation.DataPropertyName = "WordsQuantityInformation";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe Print", 14.25F);
            this.WordsQuantityInformation.DefaultCellStyle = dataGridViewCellStyle8;
            this.WordsQuantityInformation.HeaderText = "Q.I";
            this.WordsQuantityInformation.Name = "WordsQuantityInformation";
            this.WordsQuantityInformation.Width = 120;
            // 
            // coldgvOccurrence
            // 
            this.coldgvOccurrence.DataPropertyName = "WordsEntropy";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Purple;
            this.coldgvOccurrence.DefaultCellStyle = dataGridViewCellStyle9;
            this.coldgvOccurrence.HeaderText = "Entropy";
            this.coldgvOccurrence.Name = "coldgvOccurrence";
            this.coldgvOccurrence.ReadOnly = true;
            this.coldgvOccurrence.Width = 130;
            // 
            // lblTotalEntropy
            // 
            this.lblTotalEntropy.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTotalEntropy.AutoSize = true;
            this.lblTotalEntropy.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalEntropy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalEntropy.ForeColor = System.Drawing.Color.White;
            this.lblTotalEntropy.Location = new System.Drawing.Point(114, 428);
            this.lblTotalEntropy.Name = "lblTotalEntropy";
            this.lblTotalEntropy.Size = new System.Drawing.Size(116, 25);
            this.lblTotalEntropy.TabIndex = 15;
            this.lblTotalEntropy.Text = "No of Chars";
            // 
            // lblAverage
            // 
            this.lblAverage.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblAverage.AutoSize = true;
            this.lblAverage.BackColor = System.Drawing.Color.Transparent;
            this.lblAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAverage.ForeColor = System.Drawing.Color.White;
            this.lblAverage.Location = new System.Drawing.Point(114, 453);
            this.lblAverage.Name = "lblAverage";
            this.lblAverage.Size = new System.Drawing.Size(116, 25);
            this.lblAverage.TabIndex = 16;
            this.lblAverage.Text = "No of Chars";
            // 
            // frmEntropy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.bk_entropy;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(708, 516);
            this.Controls.Add(this.lblAverage);
            this.Controls.Add(this.lblTotalEntropy);
            this.Controls.Add(this.dgvLettersEntropy);
            this.Controls.Add(this.dgvWordsProbability);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEntropy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Entropy";
            this.Load += new System.EventHandler(this.frmEntropy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLettersEntropy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWordsProbability)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLettersEntropy;
        private System.Windows.Forms.DataGridView dgvWordsProbability;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLetters;
        private System.Windows.Forms.DataGridViewTextBoxColumn LetterQuantityInformation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldgvWords;
        private System.Windows.Forms.DataGridViewTextBoxColumn WordsQuantityInformation;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldgvOccurrence;
        private System.Windows.Forms.Label lblTotalEntropy;
        private System.Windows.Forms.Label lblAverage;


    }
}