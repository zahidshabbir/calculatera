﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncrytpionDecryption
{
    public partial class frmEntropy : Form
    {
        public static List<ClassLettersEntropy> ListOfLettersEntropy;
        public static List<ClassWordsEntropy> ListOfWordsEntropy;
        public static bool ShowLettersEntropy;
        public static bool ShowWordsEntropy;
        public static string TotalWordsEntropy;
        public static string TotalLettersEntropy;
        public static double AverageOfLettersEntropi;
        public static double AverageOfWordsEntropi;


        public frmEntropy ()
        {
            InitializeComponent();
        }

        private void dgvLettersProbability_CellContentClick ( object sender, DataGridViewCellEventArgs e )
        {

        }

        private void frmEntropy_Load ( object sender, EventArgs e )
        {
            if(ShowLettersEntropy == true && ShowWordsEntropy == false)
         
            {
                lblTotalEntropy.Text = string.Empty;
                dgvLettersEntropy.Visible = true;
                dgvLettersEntropy.DataSource = ListOfLettersEntropy;
                dgvWordsProbability.Visible = false;
                lblTotalEntropy.Text = "Entropia Total: ";
                lblTotalEntropy.Text += TotalLettersEntropy;
                lblAverage.Text = "Entropia Média: " + Math.Round( AverageOfLettersEntropi, 4 );
            }
            else
                if ( ShowWordsEntropy == true && ShowLettersEntropy == false )
                {
                    lblTotalEntropy.Text = string.Empty;
                    dgvLettersEntropy.Visible = false;
                    dgvWordsProbability.Visible = true;
                    dgvWordsProbability.DataSource = ListOfWordsEntropy;
                    lblTotalEntropy.Text = "Entropia Total: ";
                    lblTotalEntropy.Text += TotalWordsEntropy;
                    lblAverage.Text = "Entropia Média: " + Math.Round( AverageOfWordsEntropi, 4 ); ;
                }
        }

        private void dgvLettersEntropy_CellContentClick ( object sender, DataGridViewCellEventArgs e )
        {

        }

        private void dgvWordsProbability_CellContentClick ( object sender, DataGridViewCellEventArgs e )
        {

        }
    }
}
