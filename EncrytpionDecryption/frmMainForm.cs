﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;
namespace EncrytpionDecryption
{
    public partial class frmMainForm : Form
    {
        Point LastClick;


        public frmMainForm ()
        {
            InitializeComponent();
        }

        private void frmMainForm_FormClosed ( object sender, FormClosedEventArgs e )
        {
            Environment.Exit( 1 );
        }
        string exMystringAfterRemovingAllSpaces;
        List<ClassWordsProbability> listOfWordsOccerence = new List<ClassWordsProbability>();
        List<ClassLettersProbability> listOfAlphabetsOccerence = new List<ClassLettersProbability>();

        List<ClassWordsEntropy> lstWordsEntropy = new List<ClassWordsEntropy>();
        List<ClassLettersEntropy> lstLettersEntropy = new List<ClassLettersEntropy>();
        bool OKButtonIsChecked;
        decimal NumberOfLetters ;
        int NumberOfWords;
        int sentences;
        List<string> lstOfEntropyOfWords = new List<string>();
        double TotalLettersEntropy = 0, TotalWordsEntropy = 0;
        double AverageOfLettersEntropy, AverageOfWordsEntropy;

        private void btnGetInfo_Click ( object sender, EventArgs e )
        {

            if ( rtbINput.Text != "" )
            {
                NumberOfLetters = 0;
                NumberOfWords = 0;
                sentences = 0;

                OKButtonIsChecked = true;
            #region No of words and Letters
            

            string str = rtbINput.Text;  // getting Text Document in a string.

            exMystringAfterRemovingAllSpaces = rtbINput.Text;    // New Logic, It will remove all empty spaces in throughout the whole document.So lets give this string to this variable

            exMystringAfterRemovingAllSpaces = String.Join( " ", exMystringAfterRemovingAllSpaces.Split( new char [] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) ); // This line is going to do the magic to remove all empty spaces.



            string [] stplitedstr = exMystringAfterRemovingAllSpaces.Trim().Split( ' ',';',':', '.' ); // To Calculate the No of words and chars, we split the words on the basis of space

            for ( int loop = 0 ; loop < stplitedstr.Length ; loop++ ) // Loop to go through the each splited string to get No of characters and  No of words
            {


                if ( stplitedstr [ loop ].ToString() != string.Empty )
                {
                    NumberOfLetters += Convert.ToInt32( stplitedstr [ loop ].Length );
                    NumberOfWords += 1;
                }



            }
            #endregion

            //string tempwordstring = rtbINput.Text;
            //string [] temparraywordsstring = tempwordstring.Trim().Split( '.' );


           lblLetters.Visible = true;
                lblSentences.Visible = true;
                lblWords.Visible = true;

                lblLetters.Text = "Número de letras: " + NumberOfLetters.ToString();
                string strFindingNuberOfSenetences = rtbINput.Text;
                lblWords.Text = "Número de palavras:" + NumberOfWords;
                 sentences = strFindingNuberOfSenetences.Split( '.' ).Length - 1;

                if ( sentences == 0 )
                    lblSentences.Text = "Não há sentenças completas.";
                else
                    lblSentences.Text = "Número de sentenças: " + sentences.ToString();
            }
            else
                MessageBox.Show( "Não há texto a ser quantificado.", "Inválido", MessageBoxButtons.OK, MessageBoxIcon.Error );

            ToggleBold();
            //MessageBox.Show( "Total No of Chars are..> " + count.ToString() );

            
        }

        private void rtbINput_TextChanged ( object sender, EventArgs e )
        {
            //MethodToUnderLineSpecificWords();
        }

        private void btnCLose_Click ( object sender, EventArgs e )
        {
            if ( MessageBox.Show( "Tu realmente desejas fechar este programa?", "Confirmaçãode saída", MessageBoxButtons.YesNo, MessageBoxIcon.Information ) == DialogResult.Yes )
            {
                this.Close();
            }
        }

        private void frmMainForm_MouseDown ( object sender, MouseEventArgs e )
        {
            LastClick = e.Location;
        }

        private void frmMainForm_MouseMove ( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                this.Left += e.X - LastClick.X;
                this.Top += e.Y - LastClick.Y;
            }
        }

        private void rtbINput_MouseMove ( object sender, MouseEventArgs e )
        {
        }

        private void button1_Click ( object sender, EventArgs e )
        {


        }

        private void btnClear_Click ( object sender, EventArgs e )
        {
            rtbINput.Text = string.Empty;
            lblWords.Text = string.Empty;
            lblLetters.Text = string.Empty;
            lblSentences.Text = string.Empty;
            dgvWordsProbability.DataSource = null;
            listOfWordsOccerence = new List<ClassWordsProbability>();
            lstWordsEntropy = new List<ClassWordsEntropy>();
            dgvLettersProbability.DataSource = null;
            listOfAlphabetsOccerence = new List<ClassLettersProbability>();
            lstLettersEntropy = new List<ClassLettersEntropy>();
            OKButtonIsChecked = false;
            NumberOfWords = 0;
            NumberOfLetters = 0;
            sentences = 0;


        }

        private void ToggleBold ()
        {
            if ( rtbINput.SelectionFont != null )
            {
                System.Drawing.Font currentFont = rtbINput.SelectionFont;
                System.Drawing.FontStyle newFontStyle;

                if ( rtbINput.SelectionFont.Bold == true )
                {
                    newFontStyle = FontStyle.Regular;
                }
                else
                {
                    newFontStyle = FontStyle.Bold;
                }

                rtbINput.SelectionFont = new Font( currentFont.FontFamily, currentFont.Size, newFontStyle );
            }
        }

        private void frmMainForm_Load ( object sender, EventArgs e )
        {
            ToggleBold();
            this.Location = new Point( 0, 0 );
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;


        }

        List<string> MyListWhichHoldNormalizedstring = new List<string>();


        int CountForMe = 0; // this count will count when a word is founded in Normalized List. If its more than One then it will not add that word otherwise it will add that word in Normalized LIst.
        //int Max = 0;
        bool CheckIfWordsProbabilityIsClicked = true;
        bool CheckIfLettersProbabilitIsClicked = true;
        private void btnResultDetail_Click ( object sender, EventArgs e )
        {
            if ( OKButtonIsChecked == true )
            {

                CheckIfWordsProbabilityIsClicked = true;
                CheckIfLettersProbabilitIsClicked = false;
                dgvLettersProbability.Visible = false;
                dgvWordsProbability.Visible = true;






 #region Getting Normalized answer list

                ///   In this section we'll add all repeated answers list ( if there is repeatition) 
                ///   in New Normalized list then
                ///   we will calculate the  Probability of those words
                    
                if ( rtbINput.Text != "" )
                {
                   



                    List<string> lstNormalizedAnser = new List<string>();
                    for ( int loop = 0 ; loop < lstSaveAnserCheckedByuser.Count ; loop++ )
                    {
                        if ( loop == 0 )
                        {
                            lstNormalizedAnser.Add( lstSaveAnserCheckedByuser [ loop ].ToString() ); // First element will be added automatically in list of Normalzed answser
                           
                        }
                        
                         else
                        {
                            CountForMe = 0;  // we'll set the CountForMe = 0 , so that if Count is zero that means it was new Element so we'll add that element in Normalized list

                            int n = lstNormalizedAnser.Count;
                            for ( int myloop = 0 ; myloop < n ; myloop++ )
                            {

                                if ( lstSaveAnserCheckedByuser [ loop ].ToUpper() == lstNormalizedAnser [ myloop ].ToUpper() )
                                {
                                    CountForMe++;   // If Count is incremented then it means Item already presented in the List so we'll go to Answer List to find Other Item to Nomalize it
                                    break;
                                }

                            }
                            if ( CountForMe == 0 )  // if Count is zero that means it was new Element so we'll add that element in Normalized list
                            {
                                //string strToAddinNormaizedList = Responses[loop];
                                if ( lstSaveAnserCheckedByuser [ loop ] != string.Empty ) // if new element is not an emtpy string then we'll save that element finally in the Normalized list
                                {
                                    lstNormalizedAnser.Add( lstSaveAnserCheckedByuser [ loop ].Trim().ToString() );
                                }

                            }
                        }

                    }
#endregion

                    
#region Caculating the No Occurrence of words in Text for Probability

                    List<int> lstOccurerrenceOfWords = new List<int>();
                    int CountedWords = 0;
                  
                    if ( listOfWordsOccerence.Count != 0 )   // If user Press again the Button to calculate the Probability
                    {


                        dgvWordsProbability.DataSource = null; // then it will reset the dattasource first then it will add new values if data in Rich text document is changed.
                        listOfWordsOccerence = new List<ClassWordsProbability>(); // Creating a new instance after reset the datasource to null.
                        lstWordsEntropy = new List<ClassWordsEntropy>();


                    }
                    for ( int OccurenceLoop = 0 ; OccurenceLoop < lstNormalizedAnser.Count ; OccurenceLoop++ )
                    {

                        string tempstr = lstNormalizedAnser [ OccurenceLoop ].ToString().ToUpper(); /// assign string in temp variable on the basis of Index value One By One
                                                                                                           /// This string ( Non repeated string value )will be checked in the
                                                                                                           /// Answer list to calculate Total No of Occurrence
                       
                        for ( int innerLoop = 0 ; innerLoop < lstSaveAnserCheckedByuser.Count ; innerLoop++ )  //This loop will go through Answer List to check How many time a no is repeated in this List
                        {
                            if ( tempstr.Trim() == lstSaveAnserCheckedByuser [ innerLoop ].ToString().ToUpper().Trim() ) // If no if found then it will increment of that String Occurence
                            {
                                CountedWords++;
                            }
                        }


                        if ( CountedWords != 0 )   // Count != 0 ; Means we've found some repeated elements
                        {

                            // Here we are calculating the words probability

                            ClassWordsProbability cl = new ClassWordsProbability(); // A class initilization that will save the Occurrenece of Words in form of Object
                            cl.Palavras = lstNormalizedAnser [ OccurenceLoop ].ToString();
                            cl.Frequência = CountedWords;
                            cl.Probabilidade = Math.Round( ( Convert.ToDecimal( CountedWords * 100.00 ) / NumberOfWords )/100, 3 );


                            listOfWordsOccerence.Add( cl );

                            /// to get the Entropy we need to use following formulas
                            /// I = log (1/p)
                            /// H = I . p
                            /// 
                            ClassWordsEntropy ClsE = new ClassWordsEntropy();
                            
                            double tempProb = Convert.ToDouble(cl.Probabilidade);
                            double QuantityInfoI = Math.Log( 1 / (tempProb ),2);
                            string FinalEntropy = (Math.Round((QuantityInfoI * tempProb),4)).ToString();
                            ClsE.WordsName = cl.Palavras;
                            ClsE.WordsEntropy = FinalEntropy;
                            ClsE.WordsQuantityInformation =Math.Round( QuantityInfoI,3).ToString();

                            lstWordsEntropy.Add( ClsE );
                           TotalWordsEntropy  += Convert.ToDouble(FinalEntropy);
                            CountedWords = 0;
                        }

                    }



                    #endregion

                    dgvWordsProbability.DataSource = listOfWordsOccerence;
                    frmEntropy.ListOfWordsEntropy = lstWordsEntropy;
                    frmEntropy.TotalWordsEntropy = TotalWordsEntropy.ToString();
                    frmEntropy.AverageOfWordsEntropi = (TotalWordsEntropy/TotalNoOfQuestion);
                    frmEntropy.ShowWordsEntropy = true;
                    frmEntropy.ShowLettersEntropy = false;
                }
                else
                    MessageBox.Show( "Por favor, digite um texto.", "Nenhum texto encontrado.", MessageBoxButtons.OK, MessageBoxIcon.Information );

            }
            else
                MessageBox.Show( "Por favor, conte as letras ou palavras para obter os seus detalhes.", "Contagem inválida.", MessageBoxButtons.OK, MessageBoxIcon.Information );
        }

        private void frmMainForm_KeyUp ( object sender, KeyEventArgs e )
        {

        }

        private void txtSearch_TextChanged ( object sender, EventArgs e )
        {
            if ( rtbINput.Text != string.Empty )
            {
                //( dgvWordsProbability.DataSource as DataTable ).DefaultView.RowFilter = string.Format( "coldgvWords = '{0}'", txtSearch.Text.ToUpper() );
                //string rowFilter = string.Format( "[{0}] = '{1}'", coldgvWords, txtSearch.Text.ToUpper() );
                //( dgvWordsProbability.DataSource as DataTable ).DefaultView.RowFilter = rowFilter;

               
                //bs.Filter = string.Format( "coldgvWords LIKE '%{0}%'", txtSearch.Text.ToUpper() );
                ////bs.Filter = coldgvWords + " like '%" + txtSearch.Text + "%'";
                //dgvWordsProbability.DataSource = bs;
                //dataGridView1.DataSource = bs;
                //dataGridView1.Refresh();
                //DataTable dt = new DataTable();
                if ( CheckIfWordsProbabilityIsClicked == true )
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = dgvWordsProbability.DataSource;
                    BindingList<ClassWordsProbability> filtered = new BindingList<ClassWordsProbability>( listOfWordsOccerence.Where( obj => obj.Palavras.Contains( txtSearch.Text.ToUpper() ) ).ToList() );

                    dgvWordsProbability.DataSource = filtered;
                    dgvWordsProbability.Update();
                }
                else 
                {
                    if ( CheckIfLettersProbabilitIsClicked == true )
                    {
                        BindingSource bsLetters = new BindingSource();
                        bsLetters.DataSource = dgvLettersProbability.DataSource;
                        BindingList<ClassLettersProbability> filtered = new BindingList<ClassLettersProbability>( listOfAlphabetsOccerence.Where( obj => obj.Letras.Contains( txtSearch.Text.ToLower() ) ).ToList() );

                        dgvLettersProbability.DataSource = filtered;
                        dgvLettersProbability.Update();
                    }
                }
            }
            else
            {
                MessageBox.Show( "Por favor, digite um texto.", "Não há texto ", MessageBoxButtons.OK, MessageBoxIcon.Information );
                txtSearch.Clear();
            }


        }

        private void roundButton2_Click ( object sender, EventArgs e )
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = dgvWordsProbability.DataSource;
            bs.Filter = coldgvWords + " like '%" + txtSearch.Text + "%'";
            dgvWordsProbability.DataSource = bs;
            DataTable dt = new DataTable();

        }

        private void btnLettersProbability_Click ( object sender, EventArgs e )
        {
           
            if(OKButtonIsChecked == true)
            {
            if ( rtbINput.Text != "" )
            {
                 CheckIfWordsProbabilityIsClicked = false;
                 CheckIfLettersProbabilitIsClicked = true;
                dgvLettersProbability.Visible = true;
                dgvWordsProbability.Visible = false;
                if ( listOfAlphabetsOccerence.Count != 0 )
                {
                    dgvLettersProbability.DataSource = null;
                    listOfAlphabetsOccerence = new List<ClassLettersProbability>();
                    lstLettersEntropy = new List<ClassLettersEntropy>();
                    
                }

                string [] PortugueseAlphabets = new string [ 26 ];
                string [] CountPortugueseAlphabets = new string [ 26 ];
                string TempPortugueseAlphabets = "a b c d e f g h i j k l m n o p q r s t u v w x y z";
                PortugueseAlphabets = TempPortugueseAlphabets.Split( ' ' );
                string tempstr;
                string strUpperTextString = rtbINput.Text.ToLower();
                int CountedLetters = 0;
                decimal Probability;
                for ( int letterloop = 0 ; letterloop < PortugueseAlphabets.Length ; letterloop++ )
                {

                    tempstr = PortugueseAlphabets [ letterloop ].ToString(); // getting Letter or char to compare with alphabets
                    if ( letterloop == 0 || letterloop == 2 || letterloop == 4 || letterloop == 8 || letterloop == 14 || letterloop == 20 )
                    {
                        int SwtichCaseVariable = letterloop;
                        switch ( SwtichCaseVariable )
                        {
                            case 0: // in this case we will match all chars or Letters and get the total of them: a á   à   ã   â
                                string [] mytempcharsA = new string [ 5 ];
                                mytempcharsA = "a á à ã â".Split( ' ' );
                                for ( int CaseLoop = 0 ; CaseLoop <= 4 ; CaseLoop++ )
                                {
                                    string tempA = mytempcharsA [ CaseLoop ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }

                                break;


                            case 2: // in this case we will match all chars or Letters and get the total of them: c ç

                                string [] mytempcharsC = new string [ 2 ];
                                mytempcharsC = "c ç".Split( ' ' );
                                for ( int loopA = 0 ; loopA <= 1 ; loopA++ )
                                {
                                    string tempA = mytempcharsC [ loopA ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }
                                break;


                            case 4:// in this case we will match all chars or Letters and get the total of them: e é ê
                                string [] mytempcharsE = new string [ 2 ];
                                mytempcharsE = "e é ê".Split( ' ' );
                                for ( int loopE = 0 ; loopE <= 2 ; loopE++ )
                                {
                                    string tempA = mytempcharsE [ loopE ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }

                                break;


                            case 8:// in this case we will match all chars or Letters and get the total of them: i í
                                string [] mytempcharsI = new string [ 2 ];
                                mytempcharsI = "i í".Split( ' ' );
                                for ( int loopI = 0 ; loopI <= 1 ; loopI++ )
                                {
                                    string tempA = mytempcharsI [ loopI ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }
                                break;

                            case 14:// in this case we will match all chars or Letters and get the total of them: o ó ô õ

                                string [] mytempcharsO = new string [ 2 ];
                                mytempcharsO = "o ó ô õ".Split( ' ' );
                                for ( int loopI = 0 ; loopI <= 3 ; loopI++ )
                                {
                                    string tempA = mytempcharsO [ loopI ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }

                                break;

                            case 20:// in this case we will match all chars or Letters and get the total of them: u ú

                                string [] mytempcharsU = new string [ 2 ];
                                mytempcharsU = "u ú".Split( ' ' );
                                for ( int loopI = 0 ; loopI <= 1 ; loopI++ )
                                {
                                    string tempA = mytempcharsU [ loopI ];

                                    foreach ( Match m in Regex.Matches( strUpperTextString, tempA ) )
                                    {
                                        CountedLetters++;
                                    }
                                }

                                break;





                            default:
                                break;
                        }
                    }
                    else
                    {



                        //if ( tempstr == "á" || tempstr == "à" || tempstr == "ã" || tempstr == "â" )
                        //{
                        //    tempstr = "a";

                        //}
                        foreach ( Match m in Regex.Matches( strUpperTextString, tempstr ) )
                        {
                            CountedLetters++;
                        }
                    }
                    ClassLettersProbability classlp = new ClassLettersProbability();
                    if ( CountedLetters != 0 )
                    {
                        classlp.Letras = PortugueseAlphabets [ letterloop ].ToString();
                       // decimal tempcount = Convert.ToDecimal(Math.Round( count, 2 ));
                        Probability = Math.Round( Convert.ToDecimal( ( CountedLetters * 100 ) / NumberOfLetters ), 2 );
                        SumofProbability += Probability;
                        classlp.Frequência = CountedLetters;
                        classlp.Probablidade = Probability/100;
                        listOfAlphabetsOccerence.Add( classlp );

                        double tempProb = Convert.ToDouble( classlp.Probablidade );
                        double QuantityInfoI = Math.Log( 1 / (tempProb) ,2);
                        string FinalEntropy = ( Math.Round( ( QuantityInfoI * tempProb ), 4 ) ).ToString();
                        ClassLettersEntropy ClsLE= new ClassLettersEntropy();  /// a class that will hold data of Entropy of Letters
                                                                               /// 


                        ClsLE.LetterName = classlp.Letras;                     /// we are saving the Name of letter with its entropy value.
                                                                               /// .
                                                                               /// 
                        ClsLE.LetterEntropy = FinalEntropy;
                        ClsLE.LetterQuantityInformation = ( Math.Round( QuantityInfoI,3 ) ).ToString();

                        lstLettersEntropy.Add( ClsLE );                        /// add that object in a list then we'll add new letter and its entropy
                                                                               /// 
                        TotalLettersEntropy +=Convert.ToDouble( FinalEntropy);
                        //frmEntropy.TotalLettersEntropy = FinalEntropy;
                        CountedLetters = 0;

                    }
                }
                dgvLettersProbability.DataSource = listOfAlphabetsOccerence;
                frmEntropy.ListOfLettersEntropy = lstLettersEntropy;
                frmEntropy.AverageOfLettersEntropi = ( TotalLettersEntropy / 26 );
                frmEntropy.TotalLettersEntropy = TotalLettersEntropy.ToString();
                frmEntropy.ShowLettersEntropy = true;
                frmEntropy.ShowWordsEntropy = false;
            }
            else
                MessageBox.Show( "Por favor, digite um texto.", "Nenhum texto encontrado.", MessageBoxButtons.OK, MessageBoxIcon.Information );

            }
            else
                MessageBox.Show( "Por favor, conte as letras ou palavras para obter os seus detalhes.", "Contagem inválida.", MessageBoxButtons.OK, MessageBoxIcon.Information );
        }
        decimal SumofProbability = 0;
        public static string IHaveTextAlready;
        public static List<string> lstSaveAnserCheckedByuser;
        private void CMSPaste_Click ( object sender, EventArgs e )
        {
            rtbINput.Text += Clipboard.GetText();
        }

        private void resetToolStripMenuItem_Click ( object sender, EventArgs e )
        {
            rtbINput.Text = string.Empty;
        }

        private void cLoseToolStripMenuItem_Click ( object sender, EventArgs e )
        {
            if ( MessageBox.Show( "Tu realmente desejas fechar este programa?", "Confirmaçãode saída", MessageBoxButtons.YesNo, MessageBoxIcon.Information ) == DialogResult.Yes )
            {
                this.Close();
               
            }
        }

        private void button1_Click_1 ( object sender, EventArgs e )
        {
            frmcalculator frmcl = new frmcalculator();
            frmcl.Show();
        }
        bool TextisUnderLine = false;
        public static int TotalNoOfQuestion;
        private void ctmLoadTextData_Click ( object sender, EventArgs e )
        {
            /* this text is the text which is obtail from the Test form and then send that data to this form*/

           

           MethodToUnderLineSpecificWords();

        }
        //bool JumptoForeachIteratiion = false;
        private void MethodToUnderLineSpecificWords ()
        {
            //rtbINput.Text = IHaveTextAlready;
            string [] strtempInut = IHaveTextAlready.Trim().Split( ' ' );
            string strBIGString = string.Empty;
            foreach ( string item in strtempInut )
            {
                string strtemp = string.Empty;
                for ( int LoopCounter = 0 ; LoopCounter < lstSaveAnserCheckedByuser.Count ; LoopCounter++ )
                {
                     strtemp = lstSaveAnserCheckedByuser [ LoopCounter ].ToString();
                    if ( item == strtemp && TextisUnderLine == false )
                    {
                        rtbINput.SelectionFont = new Font( "Segoe Print", 22, FontStyle.Underline );
                        TextisUnderLine = true;
                        rtbINput.SelectedText = item + " ";
                        //rtbINput.SelectedText = item +" ";
                        rtbINput.SelectionFont = new Font( "Segoe Print", 18, FontStyle.Regular );
                        //TextisUnderLine = false;
                        //break;

                        LoopCounter = 33;

                    }
                    
                }

                if ( item != strtemp  )
                {
                    rtbINput.SelectionFont = new Font( "Segoe Print", 18, FontStyle.Regular );
                TextisUnderLine = false;
                rtbINput.SelectedText = item +" ";
                }
            }
        }

        private void btnLettersEntropy_Click ( object sender, EventArgs e )
        {
            frmEntropy frme = new frmEntropy();
            frme.ShowDialog();
        }

        private void dgvLettersProbability_CellContentClick ( object sender, DataGridViewCellEventArgs e )
        {

        }

        private void dgvWordsProbability_CellContentClick ( object sender, DataGridViewCellEventArgs e )
        {

        }
    }
}
