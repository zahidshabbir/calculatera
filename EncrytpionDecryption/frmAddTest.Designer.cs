﻿namespace EncrytpionDecryption
{
    partial class frmAddTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.grbChoices = new System.Windows.Forms.GroupBox();
            this.btnCreateNewTest = new System.Windows.Forms.Button();
            this.txtQuestionArea = new System.Windows.Forms.TextBox();
            this.btnStartTest = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grbChoices.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbChoices
            // 
            this.grbChoices.Controls.Add(this.label4);
            this.grbChoices.Controls.Add(this.label2);
            this.grbChoices.Controls.Add(this.label3);
            this.grbChoices.Controls.Add(this.label1);
            this.grbChoices.Controls.Add(this.textBox4);
            this.grbChoices.Controls.Add(this.textBox3);
            this.grbChoices.Controls.Add(this.textBox2);
            this.grbChoices.Controls.Add(this.textBox1);
            this.grbChoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbChoices.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.grbChoices.Location = new System.Drawing.Point(46, 167);
            this.grbChoices.Name = "grbChoices";
            this.grbChoices.Size = new System.Drawing.Size(733, 192);
            this.grbChoices.TabIndex = 4;
            this.grbChoices.TabStop = false;
            this.grbChoices.Text = "Por favor Give appropriate Choices";
            // 
            // btnCreateNewTest
            // 
            this.btnCreateNewTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewTest.ForeColor = System.Drawing.Color.Purple;
            this.btnCreateNewTest.Location = new System.Drawing.Point(499, 374);
            this.btnCreateNewTest.Name = "btnCreateNewTest";
            this.btnCreateNewTest.Size = new System.Drawing.Size(159, 38);
            this.btnCreateNewTest.TabIndex = 2;
            this.btnCreateNewTest.Text = "Add Test";
            this.btnCreateNewTest.UseVisualStyleBackColor = true;
            // 
            // txtQuestionArea
            // 
            this.txtQuestionArea.Location = new System.Drawing.Point(21, 20);
            this.txtQuestionArea.Multiline = true;
            this.txtQuestionArea.Name = "txtQuestionArea";
            this.txtQuestionArea.Size = new System.Drawing.Size(758, 88);
            this.txtQuestionArea.TabIndex = 3;
            // 
            // btnStartTest
            // 
            this.btnStartTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartTest.ForeColor = System.Drawing.Color.Purple;
            this.btnStartTest.Location = new System.Drawing.Point(330, 180);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(159, 38);
            this.btnStartTest.TabIndex = 5;
            this.btnStartTest.Text = "Start Test";
            this.btnStartTest.UseVisualStyleBackColor = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Purple;
            this.btnSubmit.Location = new System.Drawing.Point(334, 374);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(159, 38);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(215, 26);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Choice";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(476, 47);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(215, 26);
            this.textBox2.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(367, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "First Choice";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(128, 98);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(215, 26);
            this.textBox3.TabIndex = 0;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(476, 98);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(215, 26);
            this.textBox4.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "First Choice";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(367, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "First Choice";
            // 
            // frmAddTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 433);
            this.Controls.Add(this.btnCreateNewTest);
            this.Controls.Add(this.grbChoices);
            this.Controls.Add(this.txtQuestionArea);
            this.Controls.Add(this.btnStartTest);
            this.Controls.Add(this.btnSubmit);
            this.Name = "frmAddTest";
            this.Text = "frmAddTest";
            this.grbChoices.ResumeLayout(false);
            this.grbChoices.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbChoices;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnCreateNewTest;
        private System.Windows.Forms.TextBox txtQuestionArea;
        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.Button btnSubmit;
    }
}