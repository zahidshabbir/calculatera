﻿namespace EncrytpionDecryption
{
    partial class frmMultipleChoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMultipleChoices));
            this.grbChoices = new System.Windows.Forms.GroupBox();
            this.lblQuestionLeft = new System.Windows.Forms.Label();
            this.lblTestTime = new System.Windows.Forms.Label();
            this.btnSeeResult = new System.Windows.Forms.Button();
            this.btnChangeBGImg = new System.Windows.Forms.Button();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.btnCreateNewTest = new System.Windows.Forms.Button();
            this.rtnFourthOption = new System.Windows.Forms.RadioButton();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.rtnSecondOption = new System.Windows.Forms.RadioButton();
            this.rtnThirdOption = new System.Windows.Forms.RadioButton();
            this.rtnFirstOption = new System.Windows.Forms.RadioButton();
            this.btnStartTest = new System.Windows.Forms.Button();
            this.TimerTestTime = new System.Windows.Forms.Timer(this.components);
            this.txtQuestionArea = new System.Windows.Forms.TextBox();
            this.grbChoices.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbChoices
            // 
            this.grbChoices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbChoices.BackColor = System.Drawing.Color.Transparent;
            this.grbChoices.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grbChoices.BackgroundImage")));
            this.grbChoices.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grbChoices.Controls.Add(this.lblQuestionLeft);
            this.grbChoices.Controls.Add(this.lblTestTime);
            this.grbChoices.Controls.Add(this.btnSeeResult);
            this.grbChoices.Controls.Add(this.btnChangeBGImg);
            this.grbChoices.Controls.Add(this.lblErrorMessage);
            this.grbChoices.Controls.Add(this.btnCreateNewTest);
            this.grbChoices.Controls.Add(this.rtnFourthOption);
            this.grbChoices.Controls.Add(this.btnSubmit);
            this.grbChoices.Controls.Add(this.rtnSecondOption);
            this.grbChoices.Controls.Add(this.rtnThirdOption);
            this.grbChoices.Controls.Add(this.rtnFirstOption);
            this.grbChoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbChoices.ForeColor = System.Drawing.Color.Crimson;
            this.grbChoices.Location = new System.Drawing.Point(-1, 142);
            this.grbChoices.Name = "grbChoices";
            this.grbChoices.Size = new System.Drawing.Size(801, 292);
            this.grbChoices.TabIndex = 1;
            this.grbChoices.TabStop = false;
            this.grbChoices.Text = "Por favor, escolha a resposta correta.";
            // 
            // lblQuestionLeft
            // 
            this.lblQuestionLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQuestionLeft.AutoSize = true;
            this.lblQuestionLeft.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionLeft.ForeColor = System.Drawing.Color.Purple;
            this.lblQuestionLeft.Location = new System.Drawing.Point(3, 257);
            this.lblQuestionLeft.Name = "lblQuestionLeft";
            this.lblQuestionLeft.Size = new System.Drawing.Size(105, 32);
            this.lblQuestionLeft.TabIndex = 6;
            this.lblQuestionLeft.Text = "label1";
            // 
            // lblTestTime
            // 
            this.lblTestTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTestTime.AutoSize = true;
            this.lblTestTime.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestTime.Location = new System.Drawing.Point(447, 104);
            this.lblTestTime.Name = "lblTestTime";
            this.lblTestTime.Size = new System.Drawing.Size(30, 32);
            this.lblTestTime.TabIndex = 5;
            this.lblTestTime.Text = ".";
            // 
            // btnSeeResult
            // 
            this.btnSeeResult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSeeResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeeResult.ForeColor = System.Drawing.Color.Purple;
            this.btnSeeResult.Location = new System.Drawing.Point(453, 55);
            this.btnSeeResult.Name = "btnSeeResult";
            this.btnSeeResult.Size = new System.Drawing.Size(316, 111);
            this.btnSeeResult.TabIndex = 0;
            this.btnSeeResult.Text = "Vamos calcular a probabilidade e a entropia?";
            this.btnSeeResult.UseVisualStyleBackColor = true;
            this.btnSeeResult.Visible = false;
            this.btnSeeResult.Click += new System.EventHandler(this.btnSeeResult_Click);
            // 
            // btnChangeBGImg
            // 
            this.btnChangeBGImg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnChangeBGImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeBGImg.ForeColor = System.Drawing.Color.Purple;
            this.btnChangeBGImg.Location = new System.Drawing.Point(487, 9);
            this.btnChangeBGImg.Name = "btnChangeBGImg";
            this.btnChangeBGImg.Size = new System.Drawing.Size(159, 38);
            this.btnChangeBGImg.TabIndex = 0;
            this.btnChangeBGImg.Text = "Change Foto";
            this.btnChangeBGImg.UseVisualStyleBackColor = true;
            this.btnChangeBGImg.Visible = false;
            this.btnChangeBGImg.Click += new System.EventHandler(this.btnSeeResult_Click);
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.AutoSize = true;
            this.lblErrorMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.lblErrorMessage.Location = new System.Drawing.Point(303, 163);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(0, 22);
            this.lblErrorMessage.TabIndex = 4;
            // 
            // btnCreateNewTest
            // 
            this.btnCreateNewTest.Font = new System.Drawing.Font("Segoe Print", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewTest.ForeColor = System.Drawing.Color.Purple;
            this.btnCreateNewTest.Location = new System.Drawing.Point(77, 107);
            this.btnCreateNewTest.Name = "btnCreateNewTest";
            this.btnCreateNewTest.Size = new System.Drawing.Size(159, 38);
            this.btnCreateNewTest.TabIndex = 2;
            this.btnCreateNewTest.Text = "Add Test";
            this.btnCreateNewTest.UseVisualStyleBackColor = true;
            this.btnCreateNewTest.Visible = false;
            this.btnCreateNewTest.Click += new System.EventHandler(this.btnCreateNewTest_Click);
            // 
            // rtnFourthOption
            // 
            this.rtnFourthOption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtnFourthOption.AutoSize = true;
            this.rtnFourthOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnFourthOption.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtnFourthOption.Location = new System.Drawing.Point(453, 189);
            this.rtnFourthOption.Name = "rtnFourthOption";
            this.rtnFourthOption.Size = new System.Drawing.Size(266, 48);
            this.rtnFourthOption.TabIndex = 3;
            this.rtnFourthOption.TabStop = true;
            this.rtnFourthOption.Text = "radioButton1";
            this.rtnFourthOption.UseVisualStyleBackColor = true;
            this.rtnFourthOption.CheckedChanged += new System.EventHandler(this.rtnFourthOption_CheckedChanged);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Purple;
            this.btnSubmit.Location = new System.Drawing.Point(596, 251);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(188, 38);
            this.btnSubmit.TabIndex = 0;
            this.btnSubmit.Text = "Próxima questão";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // rtnSecondOption
            // 
            this.rtnSecondOption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtnSecondOption.AutoSize = true;
            this.rtnSecondOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnSecondOption.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtnSecondOption.Location = new System.Drawing.Point(453, 53);
            this.rtnSecondOption.Name = "rtnSecondOption";
            this.rtnSecondOption.Size = new System.Drawing.Size(266, 48);
            this.rtnSecondOption.TabIndex = 1;
            this.rtnSecondOption.TabStop = true;
            this.rtnSecondOption.Text = "radioButton1";
            this.rtnSecondOption.UseVisualStyleBackColor = true;
            this.rtnSecondOption.CheckedChanged += new System.EventHandler(this.rtnSecondOption_CheckedChanged);
            // 
            // rtnThirdOption
            // 
            this.rtnThirdOption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtnThirdOption.AutoSize = true;
            this.rtnThirdOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnThirdOption.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtnThirdOption.Location = new System.Drawing.Point(119, 189);
            this.rtnThirdOption.Name = "rtnThirdOption";
            this.rtnThirdOption.Size = new System.Drawing.Size(266, 48);
            this.rtnThirdOption.TabIndex = 2;
            this.rtnThirdOption.TabStop = true;
            this.rtnThirdOption.Text = "radioButton1";
            this.rtnThirdOption.UseVisualStyleBackColor = true;
            this.rtnThirdOption.CheckedChanged += new System.EventHandler(this.rtnThirdOption_CheckedChanged);
            // 
            // rtnFirstOption
            // 
            this.rtnFirstOption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtnFirstOption.AutoSize = true;
            this.rtnFirstOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnFirstOption.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtnFirstOption.Location = new System.Drawing.Point(119, 53);
            this.rtnFirstOption.Name = "rtnFirstOption";
            this.rtnFirstOption.Size = new System.Drawing.Size(266, 48);
            this.rtnFirstOption.TabIndex = 0;
            this.rtnFirstOption.TabStop = true;
            this.rtnFirstOption.Text = "radioButton1";
            this.rtnFirstOption.UseVisualStyleBackColor = true;
            this.rtnFirstOption.CheckedChanged += new System.EventHandler(this.rtnFirstOption_CheckedChanged);
            // 
            // btnStartTest
            // 
            this.btnStartTest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartTest.ForeColor = System.Drawing.Color.Purple;
            this.btnStartTest.Location = new System.Drawing.Point(321, 197);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(178, 38);
            this.btnStartTest.TabIndex = 2;
            this.btnStartTest.Text = "Comece o teste";
            this.btnStartTest.UseVisualStyleBackColor = true;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // TimerTestTime
            // 
            this.TimerTestTime.Interval = 1000;
            this.TimerTestTime.Tick += new System.EventHandler(this.TimerTestTime_Tick);
            // 
            // txtQuestionArea
            // 
            this.txtQuestionArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuestionArea.BackColor = System.Drawing.Color.Purple;
            this.txtQuestionArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuestionArea.ForeColor = System.Drawing.Color.White;
            this.txtQuestionArea.Location = new System.Drawing.Point(-1, 2);
            this.txtQuestionArea.Multiline = true;
            this.txtQuestionArea.Name = "txtQuestionArea";
            this.txtQuestionArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQuestionArea.Size = new System.Drawing.Size(801, 135);
            this.txtQuestionArea.TabIndex = 3;
            // 
            // frmMultipleChoices
            // 
            this.AcceptButton = this.btnSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 433);
            this.Controls.Add(this.txtQuestionArea);
            this.Controls.Add(this.btnStartTest);
            this.Controls.Add(this.grbChoices);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmMultipleChoices";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MCQS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMultipleChoices_Load);
            this.grbChoices.ResumeLayout(false);
            this.grbChoices.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbChoices;
        private System.Windows.Forms.RadioButton rtnFourthOption;
        private System.Windows.Forms.RadioButton rtnSecondOption;
        private System.Windows.Forms.RadioButton rtnThirdOption;
        private System.Windows.Forms.RadioButton rtnFirstOption;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.Button btnCreateNewTest;
        private System.Windows.Forms.Label lblErrorMessage;
        private System.Windows.Forms.Button btnSeeResult;
        private System.Windows.Forms.Label lblTestTime;
        private System.Windows.Forms.Timer TimerTestTime;
        private System.Windows.Forms.Label lblQuestionLeft;
        private System.Windows.Forms.Button btnChangeBGImg;
        private System.Windows.Forms.TextBox txtQuestionArea;
    }
}