﻿namespace EncrytpionDecryption
{
    partial class frmQuestionMaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.lblQuestion = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.lblChoice1 = new System.Windows.Forms.Label();
            this.txtChoice1 = new System.Windows.Forms.TextBox();
            this.rtnAnswer1 = new System.Windows.Forms.RadioButton();
            this.lblChoice2 = new System.Windows.Forms.Label();
            this.txtChoice2 = new System.Windows.Forms.TextBox();
            this.rtnAnswer2 = new System.Windows.Forms.RadioButton();
            this.lblChoice3 = new System.Windows.Forms.Label();
            this.txtChoice3 = new System.Windows.Forms.TextBox();
            this.rtnAnswer3 = new System.Windows.Forms.RadioButton();
            this.lblChoice4 = new System.Windows.Forms.Label();
            this.txtChoice4 = new System.Windows.Forms.TextBox();
            this.rtnAnswer4 = new System.Windows.Forms.RadioButton();
            this.btnCLose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblQuestion
            // 
            this.lblQuestion.AutoSize = true;
            this.lblQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestion.Location = new System.Drawing.Point(41, 52);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(73, 20);
            this.lblQuestion.TabIndex = 0;
            this.lblQuestion.Text = "Question";
            // 
            // txtQuestion
            // 
            this.txtQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuestion.Location = new System.Drawing.Point(139, 38);
            this.txtQuestion.Multiline = true;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQuestion.Size = new System.Drawing.Size(419, 49);
            this.txtQuestion.TabIndex = 1;
            // 
            // lblChoice1
            // 
            this.lblChoice1.AutoSize = true;
            this.lblChoice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoice1.Location = new System.Drawing.Point(41, 108);
            this.lblChoice1.Name = "lblChoice1";
            this.lblChoice1.Size = new System.Drawing.Size(73, 20);
            this.lblChoice1.TabIndex = 2;
            this.lblChoice1.Text = "Question";
            // 
            // txtChoice1
            // 
            this.txtChoice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice1.Location = new System.Drawing.Point(139, 105);
            this.txtChoice1.Name = "txtChoice1";
            this.txtChoice1.Size = new System.Drawing.Size(343, 26);
            this.txtChoice1.TabIndex = 3;
            // 
            // rtnAnswer1
            // 
            this.rtnAnswer1.AutoSize = true;
            this.rtnAnswer1.Checked = true;
            this.rtnAnswer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnAnswer1.Location = new System.Drawing.Point(488, 107);
            this.rtnAnswer1.Name = "rtnAnswer1";
            this.rtnAnswer1.Size = new System.Drawing.Size(80, 24);
            this.rtnAnswer1.TabIndex = 10;
            this.rtnAnswer1.TabStop = true;
            this.rtnAnswer1.Text = "Answer";
            this.rtnAnswer1.UseVisualStyleBackColor = true;
            // 
            // lblChoice2
            // 
            this.lblChoice2.AutoSize = true;
            this.lblChoice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoice2.Location = new System.Drawing.Point(41, 152);
            this.lblChoice2.Name = "lblChoice2";
            this.lblChoice2.Size = new System.Drawing.Size(73, 20);
            this.lblChoice2.TabIndex = 4;
            this.lblChoice2.Text = "Question";
            // 
            // txtChoice2
            // 
            this.txtChoice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice2.Location = new System.Drawing.Point(139, 149);
            this.txtChoice2.Name = "txtChoice2";
            this.txtChoice2.Size = new System.Drawing.Size(343, 26);
            this.txtChoice2.TabIndex = 5;
            // 
            // rtnAnswer2
            // 
            this.rtnAnswer2.AutoSize = true;
            this.rtnAnswer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnAnswer2.Location = new System.Drawing.Point(488, 151);
            this.rtnAnswer2.Name = "rtnAnswer2";
            this.rtnAnswer2.Size = new System.Drawing.Size(80, 24);
            this.rtnAnswer2.TabIndex = 11;
            this.rtnAnswer2.TabStop = true;
            this.rtnAnswer2.Text = "Answer";
            this.rtnAnswer2.UseVisualStyleBackColor = true;
            // 
            // lblChoice3
            // 
            this.lblChoice3.AutoSize = true;
            this.lblChoice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoice3.Location = new System.Drawing.Point(41, 196);
            this.lblChoice3.Name = "lblChoice3";
            this.lblChoice3.Size = new System.Drawing.Size(73, 20);
            this.lblChoice3.TabIndex = 6;
            this.lblChoice3.Text = "Question";
            // 
            // txtChoice3
            // 
            this.txtChoice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice3.Location = new System.Drawing.Point(139, 193);
            this.txtChoice3.Name = "txtChoice3";
            this.txtChoice3.Size = new System.Drawing.Size(343, 26);
            this.txtChoice3.TabIndex = 7;
            // 
            // rtnAnswer3
            // 
            this.rtnAnswer3.AutoSize = true;
            this.rtnAnswer3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnAnswer3.Location = new System.Drawing.Point(488, 195);
            this.rtnAnswer3.Name = "rtnAnswer3";
            this.rtnAnswer3.Size = new System.Drawing.Size(80, 24);
            this.rtnAnswer3.TabIndex = 12;
            this.rtnAnswer3.TabStop = true;
            this.rtnAnswer3.Text = "Answer";
            this.rtnAnswer3.UseVisualStyleBackColor = true;
            // 
            // lblChoice4
            // 
            this.lblChoice4.AutoSize = true;
            this.lblChoice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoice4.Location = new System.Drawing.Point(41, 240);
            this.lblChoice4.Name = "lblChoice4";
            this.lblChoice4.Size = new System.Drawing.Size(73, 20);
            this.lblChoice4.TabIndex = 8;
            this.lblChoice4.Text = "Question";
            // 
            // txtChoice4
            // 
            this.txtChoice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice4.Location = new System.Drawing.Point(139, 237);
            this.txtChoice4.Name = "txtChoice4";
            this.txtChoice4.Size = new System.Drawing.Size(343, 26);
            this.txtChoice4.TabIndex = 9;
            // 
            // rtnAnswer4
            // 
            this.rtnAnswer4.AutoSize = true;
            this.rtnAnswer4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtnAnswer4.Location = new System.Drawing.Point(488, 239);
            this.rtnAnswer4.Name = "rtnAnswer4";
            this.rtnAnswer4.Size = new System.Drawing.Size(80, 24);
            this.rtnAnswer4.TabIndex = 13;
            this.rtnAnswer4.TabStop = true;
            this.rtnAnswer4.Text = "Answer";
            this.rtnAnswer4.UseVisualStyleBackColor = true;
            // 
            // btnCLose
            // 
            this.btnCLose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCLose.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.cLose;
            this.btnCLose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCLose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCLose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCLose.Location = new System.Drawing.Point(263, 273);
            this.btnCLose.Name = "btnCLose";
            this.btnCLose.Size = new System.Drawing.Size(125, 127);
            this.btnCLose.TabIndex = 15;
            this.btnCLose.UseVisualStyleBackColor = true;
            this.btnCLose.Click += new System.EventHandler(this.btnCLose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(77, 269);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 127);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmQuestionMaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.bk_entropy;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(620, 421);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCLose);
            this.Controls.Add(this.rtnAnswer4);
            this.Controls.Add(this.rtnAnswer3);
            this.Controls.Add(this.rtnAnswer2);
            this.Controls.Add(this.rtnAnswer1);
            this.Controls.Add(this.txtChoice4);
            this.Controls.Add(this.txtChoice3);
            this.Controls.Add(this.txtChoice2);
            this.Controls.Add(this.txtChoice1);
            this.Controls.Add(this.lblChoice4);
            this.Controls.Add(this.lblChoice3);
            this.Controls.Add(this.lblChoice2);
            this.Controls.Add(this.lblChoice1);
            this.Controls.Add(this.txtQuestion);
            this.Controls.Add(this.lblQuestion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQuestionMaker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmQuestionMaker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblQuestion;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Label lblChoice1;
        private System.Windows.Forms.TextBox txtChoice1;
        private System.Windows.Forms.RadioButton rtnAnswer1;
        private System.Windows.Forms.Label lblChoice2;
        private System.Windows.Forms.TextBox txtChoice2;
        private System.Windows.Forms.RadioButton rtnAnswer2;
        private System.Windows.Forms.Label lblChoice3;
        private System.Windows.Forms.TextBox txtChoice3;
        private System.Windows.Forms.RadioButton rtnAnswer3;
        private System.Windows.Forms.Label lblChoice4;
        private System.Windows.Forms.TextBox txtChoice4;
        private System.Windows.Forms.RadioButton rtnAnswer4;
        private System.Windows.Forms.Button btnCLose;
        private System.Windows.Forms.Button btnSave;
    }
}