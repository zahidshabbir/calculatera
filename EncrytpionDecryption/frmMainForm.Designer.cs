﻿namespace EncrytpionDecryption
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainForm));
            this.rtbINput = new System.Windows.Forms.RichTextBox();
            this.CTMSPaste = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMSPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLoseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctmLoadTextData = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGetInfo = new System.Windows.Forms.Button();
            this.lblLetters = new System.Windows.Forms.Label();
            this.lblWords = new System.Windows.Forms.Label();
            this.lblSentences = new System.Windows.Forms.Label();
            this.btnCLose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dgvWordsProbability = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dgvLettersProbability = new System.Windows.Forms.DataGridView();
            this.coldgvWords = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldgvOccurrence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldgvProbability = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLettersEntropy = new RoundButton();
            this.btnLettersProbability = new RoundButton();
            this.btnResultDetail = new RoundButton();
            this.colLetters = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTMSPaste.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWordsProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLettersProbability)).BeginInit();
            this.SuspendLayout();
            // 
            // rtbINput
            // 
            this.rtbINput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbINput.BackColor = System.Drawing.Color.White;
            this.rtbINput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbINput.ContextMenuStrip = this.CTMSPaste;
            this.rtbINput.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbINput.ForeColor = System.Drawing.Color.Purple;
            this.rtbINput.Location = new System.Drawing.Point(7, 9);
            this.rtbINput.Name = "rtbINput";
            this.rtbINput.Size = new System.Drawing.Size(623, 241);
            this.rtbINput.TabIndex = 0;
            this.rtbINput.Text = "";
            this.rtbINput.TextChanged += new System.EventHandler(this.rtbINput_TextChanged);
            this.rtbINput.MouseMove += new System.Windows.Forms.MouseEventHandler(this.rtbINput_MouseMove);
            // 
            // CTMSPaste
            // 
            this.CTMSPaste.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMSPaste,
            this.resetToolStripMenuItem,
            this.cLoseToolStripMenuItem,
            this.ctmLoadTextData});
            this.CTMSPaste.Name = "CTMSPaste";
            this.CTMSPaste.Size = new System.Drawing.Size(153, 92);
            // 
            // CMSPaste
            // 
            this.CMSPaste.Name = "CMSPaste";
            this.CMSPaste.Size = new System.Drawing.Size(152, 22);
            this.CMSPaste.Text = "Colar";
            this.CMSPaste.Click += new System.EventHandler(this.CMSPaste_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.resetToolStripMenuItem.Text = "Apagar";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // cLoseToolStripMenuItem
            // 
            this.cLoseToolStripMenuItem.Name = "cLoseToolStripMenuItem";
            this.cLoseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cLoseToolStripMenuItem.Text = "Fechar";
            this.cLoseToolStripMenuItem.Click += new System.EventHandler(this.cLoseToolStripMenuItem_Click);
            // 
            // ctmLoadTextData
            // 
            this.ctmLoadTextData.Name = "ctmLoadTextData";
            this.ctmLoadTextData.Size = new System.Drawing.Size(152, 22);
            this.ctmLoadTextData.Text = "Load Text Data";
            this.ctmLoadTextData.Click += new System.EventHandler(this.ctmLoadTextData_Click);
            // 
            // btnGetInfo
            // 
            this.btnGetInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetInfo.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.o1;
            this.btnGetInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnGetInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetInfo.Location = new System.Drawing.Point(258, 259);
            this.btnGetInfo.Name = "btnGetInfo";
            this.btnGetInfo.Size = new System.Drawing.Size(125, 127);
            this.btnGetInfo.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnGetInfo, "Aperte para ver o resultado.");
            this.btnGetInfo.UseVisualStyleBackColor = true;
            this.btnGetInfo.Click += new System.EventHandler(this.btnGetInfo_Click);
            // 
            // lblLetters
            // 
            this.lblLetters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLetters.AutoSize = true;
            this.lblLetters.BackColor = System.Drawing.Color.Transparent;
            this.lblLetters.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLetters.ForeColor = System.Drawing.Color.Black;
            this.lblLetters.Location = new System.Drawing.Point(9, 273);
            this.lblLetters.Name = "lblLetters";
            this.lblLetters.Size = new System.Drawing.Size(116, 25);
            this.lblLetters.TabIndex = 4;
            this.lblLetters.Text = "No of Chars";
            this.lblLetters.Visible = false;
            // 
            // lblWords
            // 
            this.lblWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblWords.AutoSize = true;
            this.lblWords.BackColor = System.Drawing.Color.Transparent;
            this.lblWords.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWords.ForeColor = System.Drawing.Color.Black;
            this.lblWords.Location = new System.Drawing.Point(9, 317);
            this.lblWords.Name = "lblWords";
            this.lblWords.Size = new System.Drawing.Size(116, 25);
            this.lblWords.TabIndex = 4;
            this.lblWords.Text = "No of Chars";
            this.lblWords.Visible = false;
            // 
            // lblSentences
            // 
            this.lblSentences.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSentences.AutoSize = true;
            this.lblSentences.BackColor = System.Drawing.Color.Transparent;
            this.lblSentences.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSentences.ForeColor = System.Drawing.Color.Black;
            this.lblSentences.Location = new System.Drawing.Point(9, 361);
            this.lblSentences.Name = "lblSentences";
            this.lblSentences.Size = new System.Drawing.Size(116, 25);
            this.lblSentences.TabIndex = 4;
            this.lblSentences.Text = "No of Chars";
            this.lblSentences.Visible = false;
            // 
            // btnCLose
            // 
            this.btnCLose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCLose.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.cLose;
            this.btnCLose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCLose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCLose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCLose.Location = new System.Drawing.Point(505, 259);
            this.btnCLose.Name = "btnCLose";
            this.btnCLose.Size = new System.Drawing.Size(125, 127);
            this.btnCLose.TabIndex = 6;
            this.toolTip1.SetToolTip(this.btnCLose, "Aperte para fechar o programa.");
            this.btnCLose.UseVisualStyleBackColor = true;
            this.btnCLose.Click += new System.EventHandler(this.btnCLose_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.r3;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(383, 259);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(125, 127);
            this.btnClear.TabIndex = 5;
            this.toolTip1.SetToolTip(this.btnClear, "Aperte para apagar o texto e recomeçar.");
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // dgvWordsProbability
            // 
            this.dgvWordsProbability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvWordsProbability.BackgroundColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWordsProbability.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvWordsProbability.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWordsProbability.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coldgvWords,
            this.coldgvOccurrence,
            this.coldgvProbability});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvWordsProbability.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvWordsProbability.GridColor = System.Drawing.Color.Blue;
            this.dgvWordsProbability.Location = new System.Drawing.Point(646, 9);
            this.dgvWordsProbability.Name = "dgvWordsProbability";
            this.dgvWordsProbability.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWordsProbability.Size = new System.Drawing.Size(378, 241);
            this.dgvWordsProbability.TabIndex = 9;
            this.dgvWordsProbability.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWordsProbability_CellContentClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::EncrytpionDecryption.Properties.Resources.npp;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(712, 361);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(312, 169);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(646, 259);
            this.txtSearch.Multiline = true;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(378, 83);
            this.txtSearch.TabIndex = 4;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // dgvLettersProbability
            // 
            this.dgvLettersProbability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLettersProbability.BackgroundColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLettersProbability.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvLettersProbability.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLettersProbability.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLetters,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLettersProbability.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvLettersProbability.GridColor = System.Drawing.Color.Blue;
            this.dgvLettersProbability.Location = new System.Drawing.Point(646, 9);
            this.dgvLettersProbability.Name = "dgvLettersProbability";
            this.dgvLettersProbability.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLettersProbability.Size = new System.Drawing.Size(378, 241);
            this.dgvLettersProbability.TabIndex = 12;
            this.dgvLettersProbability.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLettersProbability_CellContentClick);
            // 
            // coldgvWords
            // 
            this.coldgvWords.DataPropertyName = "Palavras";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe Print", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Purple;
            this.coldgvWords.DefaultCellStyle = dataGridViewCellStyle2;
            this.coldgvWords.HeaderText = "Palavras";
            this.coldgvWords.Name = "coldgvWords";
            this.coldgvWords.ReadOnly = true;
            this.coldgvWords.Width = 140;
            // 
            // coldgvOccurrence
            // 
            this.coldgvOccurrence.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.coldgvOccurrence.DataPropertyName = "Frequência";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Purple;
            this.coldgvOccurrence.DefaultCellStyle = dataGridViewCellStyle3;
            this.coldgvOccurrence.HeaderText = "Frequência";
            this.coldgvOccurrence.Name = "coldgvOccurrence";
            this.coldgvOccurrence.ReadOnly = true;
            // 
            // coldgvProbability
            // 
            this.coldgvProbability.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.coldgvProbability.DataPropertyName = "Probabilidade";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Purple;
            this.coldgvProbability.DefaultCellStyle = dataGridViewCellStyle4;
            this.coldgvProbability.HeaderText = "Probabilidade";
            this.coldgvProbability.Name = "coldgvProbability";
            // 
            // btnLettersEntropy
            // 
            this.btnLettersEntropy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLettersEntropy.Font = new System.Drawing.Font("Segoe Print", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLettersEntropy.Location = new System.Drawing.Point(36, 382);
            this.btnLettersEntropy.Name = "btnLettersEntropy";
            this.btnLettersEntropy.Size = new System.Drawing.Size(181, 99);
            this.btnLettersEntropy.TabIndex = 13;
            this.btnLettersEntropy.Text = "entropy";
            this.btnLettersEntropy.UseVisualStyleBackColor = true;
            this.btnLettersEntropy.Click += new System.EventHandler(this.btnLettersEntropy_Click);
            // 
            // btnLettersProbability
            // 
            this.btnLettersProbability.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLettersProbability.Font = new System.Drawing.Font("Segoe Print", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLettersProbability.Location = new System.Drawing.Point(449, 392);
            this.btnLettersProbability.Name = "btnLettersProbability";
            this.btnLettersProbability.Size = new System.Drawing.Size(181, 99);
            this.btnLettersProbability.TabIndex = 3;
            this.btnLettersProbability.Text = "probabilidade das letras";
            this.btnLettersProbability.UseVisualStyleBackColor = true;
            this.btnLettersProbability.Click += new System.EventHandler(this.btnLettersProbability_Click);
            // 
            // btnResultDetail
            // 
            this.btnResultDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResultDetail.Font = new System.Drawing.Font("Segoe Print", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResultDetail.Location = new System.Drawing.Point(265, 392);
            this.btnResultDetail.Name = "btnResultDetail";
            this.btnResultDetail.Size = new System.Drawing.Size(181, 99);
            this.btnResultDetail.TabIndex = 2;
            this.btnResultDetail.Text = "probabilidade das palavras";
            this.btnResultDetail.UseVisualStyleBackColor = true;
            this.btnResultDetail.Click += new System.EventHandler(this.btnResultDetail_Click);
            // 
            // colLetters
            // 
            this.colLetters.DataPropertyName = "Letras";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe Print", 14.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Purple;
            this.colLetters.DefaultCellStyle = dataGridViewCellStyle7;
            this.colLetters.HeaderText = "Letras";
            this.colLetters.Name = "colLetters";
            this.colLetters.ReadOnly = true;
            this.colLetters.Width = 140;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Frequência";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Purple;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn2.HeaderText = "Frequência";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Probablidade";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Purple;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn3.HeaderText = "Probablidade";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 110;
            // 
            // frmMainForm
            // 
            this.AcceptButton = this.btnGetInfo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.btnCLose;
            this.ClientSize = new System.Drawing.Size(1036, 493);
            this.Controls.Add(this.btnLettersEntropy);
            this.Controls.Add(this.dgvLettersProbability);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dgvWordsProbability);
            this.Controls.Add(this.btnLettersProbability);
            this.Controls.Add(this.btnResultDetail);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCLose);
            this.Controls.Add(this.lblLetters);
            this.Controls.Add(this.btnGetInfo);
            this.Controls.Add(this.rtbINput);
            this.Controls.Add(this.lblWords);
            this.Controls.Add(this.lblSentences);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Meu Amor\'s Routine";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMainForm_FormClosed);
            this.Load += new System.EventHandler(this.frmMainForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmMainForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMainForm_MouseMove);
            this.CTMSPaste.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWordsProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLettersProbability)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbINput;
        private System.Windows.Forms.Button btnGetInfo;
        private System.Windows.Forms.Label lblLetters;
        private System.Windows.Forms.Label lblWords;
        private System.Windows.Forms.Label lblSentences;
        private System.Windows.Forms.Button btnCLose;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ToolTip toolTip1;
        private RoundButton btnResultDetail;
        private System.Windows.Forms.DataGridView dgvWordsProbability;
        private RoundButton btnLettersProbability;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView dgvLettersProbability;
        private System.Windows.Forms.ContextMenuStrip CTMSPaste;
        private System.Windows.Forms.ToolStripMenuItem CMSPaste;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLoseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ctmLoadTextData;
        private RoundButton btnLettersEntropy;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldgvWords;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldgvOccurrence;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldgvProbability;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLetters;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;


    }
}