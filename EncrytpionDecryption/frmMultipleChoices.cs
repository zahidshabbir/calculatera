﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Controls;


namespace EncrytpionDecryption
{
    public partial class frmMultipleChoices : Form
    {
        
        bool MCQSBackGroundEnabedOrNot = false;
        public frmMultipleChoices ()
        {
            InitializeComponent();
             //frm1 = new frmMultipleChoices();

            //frm1.BackgroundImage = EncrytpionDecryption.Properties.Resources.zahid;

           
        }

        //private void SetLinearGradientUnderline ()
        //{
        //    // Create an underline text decoration. Default is underline.
        //    TextDecoration myUnderline = new TextDecoration();

        //    // Create a linear gradient pen for the text decoration.
        //    Pen myPen = new Pen();

        //    myPen.Brush = new LinearGradientBrush( Colors.Yellow, Colors.Red, new Point( 0, 0.5 ), new Point( 1, 0.5 ) );
        //    myPen.Brush.Opacity = 0.5;
        //    myPen.Thickness = 1.5;
        //    myPen.DashStyle = DashStyles.Dash;
        //    myUnderline.Pen = myPen;
        //    myUnderline.PenThicknessUnit = TextDecorationUnit.FontRecommended;

        //    // Set the underline decoration to a TextDecorationCollection and add it to the text block.
        //    TextDecorationCollection myCollection = new TextDecorationCollection();
        //    myCollection.Add( myUnderline );
        //    TextBlock3.TextDecorations = myCollection;
        //}
        private void frmMultipleChoices_Load ( object sender, EventArgs e )
        {

            if ( MCQSBackGroundEnabedOrNot == false )
            {
                

                this.BackgroundImage = (System.Drawing.Image)EncrytpionDecryption.Properties.Resources.zahid;
                //frm1.BackgroundImage = zahid;
               
            }
            List<string> lstMCQS = new List<string>();
            /// Setting a test format to display to user
            /// test will start when user will press this : start test Button
            /// 
           
            
          
            //FileStream fs = new FileStream();
            ReadingQuestionAndChoices();


            grbChoices.Visible = false;
            txtQuestionArea.Visible = false;
            btnSubmit.Visible = false;
            


   
        }
        string [] QuestionFile;
         string mcqstextfilestring  ;
         string strCorrectAnswers;
         string [] MCQSFile;
         string [] SubMCQSOptions;
         string QuestionTextFilestring = string.Empty;
         //int loop = 0;
         int QuestionNo = 0;
        private void btnStartTest_Click ( object sender, EventArgs e )
        {
            this.BackgroundImage = null;
            TimerTestTime.Enabled = true;
            QuestionFile = QuestionTextFilestring.Trim().Split( '?' ); // reading and separating Question On the Basis of ? char

            frmMainForm.TotalNoOfQuestion = QuestionFile.Length-1; 
            txtQuestionArea.Text = QuestionFile [ QuestionNo ].Trim().ToString();
           
           
             MCQSFile = new string [ mcqstextfilestring.Length ];
           
            MCQSFile = mcqstextfilestring.Trim().Split( '?' );
            
            {
                string tempstr = MCQSFile [ QuestionNo ].ToString();
                SubMCQSOptions = tempstr.Trim().Split( '_' );
                if( QuestionNo < MCQSFile.Length-1 )
                {
                    rtnFirstOption.Text = SubMCQSOptions [ 0 ].ToString();
                    rtnSecondOption.Text = SubMCQSOptions [ 1 ].ToString();
                    rtnThirdOption.Text = SubMCQSOptions [ 2 ].ToString();
                    rtnFourthOption.Text = SubMCQSOptions [ 3 ].ToString();
                    QuestionNo++;

                }
            }
            

            grbChoices.Visible = true;
            txtQuestionArea.Visible = true;
            btnSubmit.Visible = true;
            this.btnStartTest.Hide();

            lblQuestionLeft.Text = "Questões restantes: " + ( QuestionFile.Length - QuestionNo - 1 );

        }
        
        public  void  MethodToUnderLineSpecificWords (string str )
        {
            //rtbINput.Text = IHaveTextAlready;
            string [] strtempInut = str.Trim().Split( '"' );
            string strBIGString = string.Empty;

            txtQuestionArea.Text = strtempInut[0].ToString();
            txtQuestionArea.Font = new Font(txtQuestionArea.Font.Name,txtQuestionArea.Font.SizeInPoints, FontStyle.Underline);
            txtQuestionArea.Text += strtempInut[1].ToString().ToUpper();
             txtQuestionArea.Font = new Font(txtQuestionArea.Font.Name,txtQuestionArea.Font.SizeInPoints, FontStyle.Regular);
             txtQuestionArea.Text += strtempInut [ 2 ].ToString();
           
        }

        private void ReadingQuestionAndChoices ()
        {

            using ( var streamReader = new StreamReader( "Questions.txt", Encoding.Default ) )
            {
                while ( streamReader.Peek() >= 0 )
                {
                    var buffer = new char [ 1 ];
                    streamReader.Read( buffer, 0, buffer.Length );
                    QuestionTextFilestring += ( buffer [ 0 ] );
                }
            }
            /* Read Choices File */

            using ( var streamReader = new StreamReader( "MCQS.txt", Encoding.Default ) )
            {
                while ( streamReader.Peek() >= 0 )
                {
                    var buffer = new char [ 1 ];
                    streamReader.Read( buffer, 0, buffer.Length );
                    mcqstextfilestring += ( buffer [ 0 ] );
                }
            }
            /* reading Correct Answers*/


            using ( var streamReader = new StreamReader( "CorrectAnswer.txt", Encoding.Default ) )
            {
                while ( streamReader.Peek() >= 0 )
                {
                    var buffer = new char [ 1 ];
                    streamReader.Read( buffer, 0, buffer.Length );
                    strCorrectAnswers += ( buffer [ 0 ] );
                }
            }
        }

        private void btnCreateNewTest_Click ( object sender, EventArgs e )
        {
            frmAddTest frmadt = new frmAddTest();
            frmadt.Show();
        }
        //int loop = 1;

        string strFinalParagraphThatwillbeProcessedInEntropy;
        List<string> lstSaveAnserCheckedByuser = new List<string>();
        private void btnSubmit_Click ( object sender, EventArgs e )
        {
            
            //loop++;
            bool CheckIfAnsIsSelecte = CheckIfUserHasSelectTheAnswer();
            if ( CheckIfAnsIsSelecte == true )
            {

                //if ( QuestionNo == 5 )
                {
                    string strFullyStructuredSentence = txtQuestionArea.Text;  /// Assign Question in a string Variable
                                                                               /// to split the string on the basis of  "
                                                                               /// to add the Selected answer to that place.
                                                                               

                    string [] strtempArray = strFullyStructuredSentence.Trim().Split( '"' );  /// Question is splitted here
                    string strAnswer = GetAnserChosedByStudent();             /// a string that save the Selected answer to check if its true or not

                    lstSaveAnserCheckedByuser.Add( strAnswer.ToUpper() );     /// Saving data in Anser List and 
                                                                              /// this word will be replaced as Synonym so we make it Uper case to appear in well form
                   
                    /* This line will generate a Proper Sentence in which Worids is replaced iwth Synonym wordsas per requirement */
                    strFinalParagraphThatwillbeProcessedInEntropy += strtempArray [ 0 ].ToString() + " " + strAnswer.ToUpper()+ " " + strtempArray [ 2 ].ToString();
                   // MessageBox.Show(strFinalParagraphThatwillbeProcessedInEntropy );
                }

            if ( QuestionNo < QuestionFile.Length - 1 && txtQuestionArea.Text != string.Empty ) /// A criteria that will check if we are out of Question or not
            {
               

                
                txtQuestionArea.Text = QuestionFile [ QuestionNo ].Trim().ToString();



                

                string tempstr = MCQSFile [ QuestionNo ].ToString();
                SubMCQSOptions = tempstr.Trim().Split( '_' );
                if ( QuestionNo < MCQSFile.Length - 1 )
                {
                    
                    // Loading the next Multiple Choices in answer portion
                    rtnFirstOption.Text = SubMCQSOptions [ 0 ].ToString();
                    rtnSecondOption.Text = SubMCQSOptions [ 1 ].ToString();
                    rtnThirdOption.Text = SubMCQSOptions [ 2 ].ToString();
                    rtnFourthOption.Text = SubMCQSOptions [ 3 ].ToString();
                    
                    ResetSomeSettingtoDefault();  // Clearing the previous state of Checked Answer

                    if ( QuestionNo == 4 )
                    {
                        
                       
                    }
                    QuestionNo++;

                }

                //loop++;
                
            }
            else
              
            {
                 TimerTestTime.Enabled = false;
                 lblTestTime.Visible = true;
                 
                //grbChoices.Visible = false;
                //txtQuestionArea.Visible = false;
                //btnSubmit.Visible = false;
                //btnSeeResult.Visible = true;
                //MessageBox.Show( "Vamos calcular a probabilidade e a entropia?","",MessageBoxButtons.OK,MessageBoxIcon.Information );
                //btnChangeBGImg.Visible = true;
                if ( MessageBox.Show( " Obrigada O teste foi finalizado. " + Environment.NewLine +
                     "Vamos calcular a probabilidade e a entropia?", "Obrigada", MessageBoxButtons.YesNo, MessageBoxIcon.Information ) == DialogResult.Yes )
                {

                    this.Hide();
                    if ( QuestionNo >= QuestionFile.Length - 1 )

                        // MessageBox.Show( strFinalParagraphThatwillbeProcessedInEntropy );
                        frmMainForm.IHaveTextAlready = strFinalParagraphThatwillbeProcessedInEntropy;
                    frmMainForm.lstSaveAnserCheckedByuser = lstSaveAnserCheckedByuser;
                    frmMainForm frmmf = new frmMainForm();
                    
                    frmmf.Show();

                }
                else
                {
                    MessageBox.Show( "obrigada por ter ficado conosco. Cuide-se", "Obrigada", MessageBoxButtons.OK, MessageBoxIcon.Information );
                    Environment.Exit( 1 );
                }

            }
            }
            else
            {
                lblErrorMessage.Text = "*  Por favor, escolha a resposta correta, para avançar.";
            }
            lblQuestionLeft.Text = "Questões restantes: " + ( QuestionFile.Length - QuestionNo - 1 );
        }

        private string GetAnserChosedByStudent ()
        {
            string result = string.Empty;
            if ( rtnFirstOption.Checked == true )
                result = rtnFirstOption.Text;
            else
                if ( rtnSecondOption.Checked == true )
                    result = rtnSecondOption.Text;
                else
                    if ( rtnThirdOption.Checked == true )
                        result = rtnThirdOption.Text;
                    else
                        if ( rtnFourthOption.Checked == true )
                            result = rtnFourthOption.Text;
            return result;
        }

        private bool CheckIfUserHasSelectTheAnswer ()
        {
            bool result = false;
            if ( rtnFirstOption.Checked == true || rtnSecondOption.Checked == true || rtnThirdOption.Checked == true || rtnFourthOption.Checked == true )
            {
                result = true;

            }
            return result;
        }

        private void ResetSomeSettingtoDefault ()
        {
            // Resetting the State of Multiple Choices
            rtnFirstOption.Checked = false;
            rtnSecondOption.Checked = false;
            rtnThirdOption.Checked = false;
            rtnFourthOption.Checked = false;

            // Resetting the ForeColor ColorState of The MCQS
            rtnFirstOption.ForeColor = SystemColors.InactiveCaptionText;
            //rtnFirstOption.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#434e54" );
            rtnSecondOption.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#434e54" );
            rtnThirdOption.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#434e54" );
            rtnFourthOption.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#434e54" );
        }


        private void rtnFirstOption_CheckedChanged ( object sender, EventArgs e )
        {
            if ( rtnFirstOption.Checked == true )
            {

                Font CustomizedFont = new Font( "Consolas", 30, FontStyle.Bold );
                rtnFirstOption.Font = CustomizedFont;
                rtnFirstOption.ForeColor = System.Drawing.Color.Purple;
                //Label1.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#22FF99" );
                lblErrorMessage.Text = string.Empty;
                
            }
            else
            {
                rtnFirstOption.Font = new Font( "Microsoft Sans Serif", 28, FontStyle.Regular );
                //ControlDark
                rtnFirstOption.ForeColor = System.Drawing.Color.Chocolate;
                //InactiveCaptionText
                
//                rtnFirstOption.ForeColor = System.Drawing.ColorTranslator.FromHtml( "#434e54" );
            }
            
        }

        private void rtnSecondOption_CheckedChanged ( object sender, EventArgs e )
        {
            if ( rtnSecondOption.Checked == true )
            {

                Font CustomizedFont = new Font( "Consolas", 30, FontStyle.Bold );
                rtnSecondOption.Font = CustomizedFont;
                rtnSecondOption.ForeColor = System.Drawing.Color.Purple;
                lblErrorMessage.Text = string.Empty;
            }
            else
            {
                rtnSecondOption.Font = new Font( "Microsoft Sans Serif", 28, FontStyle.Regular );
                rtnSecondOption.ForeColor = System.Drawing.Color.Chocolate;
            }
        }

        private void rtnThirdOption_CheckedChanged ( object sender, EventArgs e )
        {
            if ( rtnThirdOption.Checked == true )
            {

                Font CustomizedFont = new Font( "Consolas", 30, FontStyle.Bold );
                rtnThirdOption.Font = CustomizedFont;
                rtnThirdOption.ForeColor = System.Drawing.Color.Purple;
                lblErrorMessage.Text = string.Empty;
            }
            else
            {
                rtnThirdOption.Font = new Font( "Microsoft Sans Serif", 28, FontStyle.Regular );
                rtnThirdOption.ForeColor = System.Drawing.Color.Chocolate;
            }
        }

        private void rtnFourthOption_CheckedChanged ( object sender, EventArgs e )
        {
            if ( rtnFourthOption.Checked == true )
            {

                Font CustomizedFont = new Font( "Consolas", 30, FontStyle.Bold );
                rtnFourthOption.Font = CustomizedFont;
                rtnFourthOption.ForeColor = System.Drawing.Color.Purple;
                lblErrorMessage.Text = string.Empty;

            }
            else
            {
                rtnFourthOption.Font = new Font( "Microsoft Sans Serif", 28, FontStyle.Regular );
                rtnFourthOption.ForeColor = System.Drawing.Color.Chocolate;
            }
        }

        private void btnSeeResult_Click ( object sender, EventArgs e )
        {
            //if ( QuestionNo >= QuestionFile.Length - 1  )
            
           // MessageBox.Show( strFinalParagraphThatwillbeProcessedInEntropy );
            frmMainForm.IHaveTextAlready = strFinalParagraphThatwillbeProcessedInEntropy;
            frmMainForm.lstSaveAnserCheckedByuser = lstSaveAnserCheckedByuser;
            frmMainForm frmmf = new frmMainForm();
            frmmf.Show();

        }
        int Seconds = 0,ss = 0;
        int Minutes = 0 , mm = 0;
        int Hours = 0, hh = 0;
        private void TimerTestTime_Tick ( object sender, EventArgs e )
        {
            Seconds++;
            if ( Seconds <9 && Hours <9 )
            {
                lblTestTime.Text = ""+hh+Hours + " : " +mm+ Minutes + " : " +ss+ Seconds;
                //lblTestTime.Text = Hours + " : " + Minutes + " : " + Seconds;
                if ( Seconds == 59 )
                {
                    Seconds = 0;
                    Minutes++; ;
                }
            }
            else
            {
                if ( Seconds > 9 && Hours < 9 )
                lblTestTime.Text = hh+""+Hours + " : "+mm + Minutes + " : " + Seconds;
                if ( Seconds == 59 )
                {
                    Seconds = 0;
                    Minutes++; ;
                }
            }
        }

        private void txtQuestionArea_Click ( object sender, EventArgs e )
        {
            
        }

        private void txtQuestionArea_TextChanged ( object sender, EventArgs e )
        {
            if ( txtQuestionArea.Text.Length <= 180 )
            {
                Font CustomizedFont = new Font( "Consolas", 20, FontStyle.Regular );
                txtQuestionArea.Font = CustomizedFont;
            }
            else
                if ( txtQuestionArea.Text.Length <= 220 && txtQuestionArea.Text.Length > 20 )
                {
                    Font CustomizedFont = new Font( "Consolas", 18, FontStyle.Regular );
                    txtQuestionArea.Font = CustomizedFont;
                }
                else
                {
                    Font CustomizedFont = new Font( "Consolas", 16, FontStyle.Regular );
                    txtQuestionArea.Font = CustomizedFont;
                }
        }
    }
}
